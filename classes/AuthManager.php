<?php

namespace RLuders\JWTAuth\Classes;

use RLuders\JWTAuth\Models\User;
use Winter\User\Classes\AuthManager as RainAuthManager;

/**
 * {@inheritDoc}
 */
class AuthManager extends RainAuthManager
{
    /**
     * {@inheritDoc}
     */
    protected $userModel = User::class;
}
